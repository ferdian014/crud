@extends('adminlte.master')

@section('title')
Edit Nama pemain
@endsection

@section('content')
<div>
   
        <form action="/cast/{{$cast->id}}" method="POST">
            @method('put')
            @csrf
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Masukkan nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">umur</label>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror

                <div class="form-group">
                    <label for="bio">bio</label>
                    <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" placeholder="Masukkan nama">
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror



            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

    @endsection