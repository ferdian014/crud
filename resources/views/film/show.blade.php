@extends('adminlte.master')

@section('title')
halaman list film
@endsection

@section('content')
    <img src="{{asset('/uploads/film/'.$film->poster)}}" alt="Card image cap">
    <h2>{{$film->judul}}</h2>
    <p>{{$film->ringkasan}}</p>

@endsection