@extends('adminlte.master')

@section('title')
edit daftar film
@endsection

@section('content')
<div>
    <h2>Tambah Data</h2>
        <form action="/film/{{$film->id}}" enctype="multipart/form-data" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label>Judul</label>
                <input type="text" class="form-control" name="judul" value={{$film->judul}} placeholder="Masukkan nama judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <div class="form-group">
                <label>Tahun</label>
                <input type="number" class="form-control" name="tahun" value={{$film->tahun}} placeholder="Masukkan tahun film">
                @error('tahun')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror

            <div class="form-group">
                <label>Ringkasan</label>
                <textarea name="ringkasan" class="form-control" cols="30" rows="10">{{$film->ringkasan}}</textarea>
                    @error('ringkasan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror

          

                <div class="form-group">
                    <label>Genre</label>
                    <select class="form-control" name="genre_id" id="exampleFormControlSelect1">
                        <option value="">--pilih genre--</option>
                        @foreach ($genre as $item)
                        @if ($item->id === $film->genre_id)
                        <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                  
                            
                        @else
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endif
                   
                        
                           
                        @endforeach
                    </select>
                     @error('genre_id')
                        <div class="alert alert-danger">
                             {{ $message }}
                        </div>
                    @enderror
                    
                    <form>
                        <div class="form-group">
                          <label>poster</label>
                          <input type="file" class="form-control-file" name="poster">
                        </div>
                      </form>
                      @error('poster')
                      <div class="alert alert-danger">
                           {{ $message }}
                      </div>
                  @enderror


            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection